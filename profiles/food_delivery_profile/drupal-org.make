; food_delivery_profile make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc6"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.2"
projects[module_filter][subdir] = "contrib"

projects[module_missing_message_fixer][version] = "1.8"
projects[module_missing_message_fixer][subdir] = "contrib"

projects[ctools][version] = "1.19"
projects[ctools][subdir] = "contrib"

projects[commerce][version] = "1.15"
projects[commerce][subdir] = "contrib"

projects[commerce_extra_price_formatters][version] = "1.1"
projects[commerce_extra_price_formatters][subdir] = "contrib"

projects[commerce_paypal][version] = "2.7"
projects[commerce_paypal][subdir] = "contrib"

projects[commerce_ajax_cart][version] = "1.0-beta3"
projects[commerce_ajax_cart][subdir] = "contrib"

projects[commerce_autosku][version] = "1.2"
projects[commerce_autosku][subdir] = "contrib"

projects[commerce_checkout_login][version] = "2.0-beta3"
projects[commerce_checkout_login][subdir] = "contrib"

projects[commerce_discount][version] = "1.0-beta5"
projects[commerce_discount][subdir] = "contrib"

projects[commerce_migrate][version] = "1.2"
projects[commerce_migrate][subdir] = "contrib"

projects[commerce_price_components][version] = "1.x-dev"
projects[commerce_price_components][subdir] = "contrib"

projects[commerce_product_display_manager][version] = "1.0-alpha4"
projects[commerce_product_display_manager][subdir] = "contrib"

projects[commerce_saleprice][version] = "1.0-beta4"
projects[commerce_saleprice][subdir] = "contrib"

projects[commerce_search_api][version] = "1.6"
projects[commerce_search_api][subdir] = "contrib"

projects[commerce_views_pane][version] = "1.0"
projects[commerce_views_pane][subdir] = "contrib"

projects[dc_cart_ajax][version] = "1.0"
projects[dc_cart_ajax][subdir] = "contrib"

projects[dc_co_pages][version] = "1.0"
projects[dc_co_pages][subdir] = "contrib"

projects[commerce_flat_rate][version] = "1.0-beta2"
projects[commerce_flat_rate][subdir] = "contrib"

projects[commerce_shipping][version] = "2.3"
projects[commerce_shipping][subdir] = "contrib"

projects[commerce_user_profile_pane][version] = "1.0-beta1"
projects[commerce_user_profile_pane][subdir] = "contrib"

projects[commerce_extra][version] = "1.0-alpha1"
projects[commerce_extra][subdir] = "contrib"

projects[context][version] = "3.10"
projects[context][subdir] = "contrib"

projects[date][version] = "2.11"
projects[date][subdir] = "contrib"

projects[devel][version] = "1.7"
projects[devel][subdir] = "contrib"

projects[migrate_extras][version] = "2.5"
projects[migrate_extras][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[commerce_features][version] = "1.4"
projects[commerce_features][subdir] = "contrib"
projects[commerce_features][patch][] = "https://www.drupal.org/files/issues/2019-04-18/features_override-commerce_checkout_pane.patch"

projects[features][version] = "2.13"
projects[features][subdir] = "contrib"

projects[uuid_features][version] = "1.0-rc2"
projects[uuid_features][subdir] = "contrib"

projects[features_extra][version] = "1.0"
projects[features_extra][subdir] = "contrib"

projects[addressfield][version] = "1.3"
projects[addressfield][subdir] = "contrib"

projects[entityreference][version] = "1.5"
projects[entityreference][subdir] = "contrib"

projects[field_group][version] = "1.6"
projects[field_group][subdir] = "contrib"

projects[field_slideshow][version] = "1.83"
projects[field_slideshow][subdir] = "contrib"

projects[inline_entity_form][version] = "1.9"
projects[inline_entity_form][subdir] = "contrib"

projects[physical][version] = "1.0"
projects[physical][subdir] = "contrib"

projects[telephone][version] = "1.0-alpha1"
projects[telephone][subdir] = "contrib"

projects[hierarchical_select][version] = "3.0-beta9"
projects[hierarchical_select][subdir] = "contrib"

projects[simplenews][version] = "1.1"
projects[simplenews][subdir] = "contrib"

projects[simplenews_realname][version] = "1.0-alpha2"
projects[simplenews_realname][subdir] = "contrib"

projects[colorized_gmap][version] = "1.5"
projects[colorized_gmap][subdir] = "contrib"

projects[migrate][version] = "2.11"
projects[migrate][subdir] = "contrib"

projects[i18n][version] = "1.31"
projects[i18n][subdir] = "contrib"

projects[path_breadcrumbs][version] = "3.4"
projects[path_breadcrumbs][subdir] = "contrib"

projects[node_export][version] = "3.1"
projects[node_export][subdir] = "contrib"

projects[block_class][version] = "2.4"
projects[block_class][subdir] = "contrib"

projects[boxes][version] = "1.2"
projects[boxes][subdir] = "contrib"

projects[colorbox][version] = "2.15"
projects[colorbox][subdir] = "contrib"

projects[entity][version] = "1.9"
projects[entity][subdir] = "contrib"

projects[jquery_touchpunch][version] = "1.0"
projects[jquery_touchpunch][subdir] = "contrib"

projects[libraries][version] = "2.5"
projects[libraries][subdir] = "contrib"

projects[menu_attributes][version] = "1.1"
projects[menu_attributes][subdir] = "contrib"

projects[profile2][version] = "1.7"
projects[profile2][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[taxonomy_display][version] = "1.1"
projects[taxonomy_display][subdir] = "contrib"

projects[token][version] = "1.8"
projects[token][subdir] = "contrib"

projects[owlcarousel][version] = "1.6"
projects[owlcarousel][subdir] = "contrib"

projects[inline_conditions][version] = "1.0"
projects[inline_conditions][subdir] = "contrib"

projects[rules][version] = "2.12"
projects[rules][subdir] = "contrib"

projects[page_title][version] = "2.7"
projects[page_title][subdir] = "contrib"

projects[commerce_search][version] = "1.0-rc6"
projects[commerce_search][subdir] = "contrib"

projects[search_api][version] = "1.28"
projects[search_api][subdir] = "contrib"

projects[search_api_db][version] = "1.8"
projects[search_api_db][subdir] = "contrib"

projects[search_api_ranges][version] = "1.x-dev"
projects[search_api_ranges][subdir] = "contrib"

projects[search_api_sorts][version] = "1.7"
projects[search_api_sorts][subdir] = "contrib"

projects[ajax_facets][version] = "3.7"
projects[ajax_facets][subdir] = "contrib"
projects[ajax_facets][patch][] = https://www.drupal.org/files/issues/ajax_facets-all-displays-use-default-display-access-2900984-08.patch
projects[ajax_facets][patch][] = https://www.drupal.org/files/issues/incorrect_path_contextual_filter-2480557-16.patch

projects[facetapi][version] = "1.6"
projects[facetapi][subdir] = "contrib"

projects[facetapi_bonus][version] = "1.3"
projects[facetapi_bonus][subdir] = "contrib"

projects[slick][version] = "2.x-dev"
projects[slick][subdir] = "contrib"
projects[slick][patch][] = "https://www.drupal.org/files/issues/2021-08-18/3228734-Change-link-to-mousewheel-library.patch"

projects[slick_views][version] = "2.2"
projects[slick_views][subdir] = "contrib"

projects[uuid][version] = "1.3"
projects[uuid][subdir] = "contrib"

projects[ckeditor][version] = "1.19"
projects[ckeditor][subdir] = "contrib"

projects[jquery_update][version] = "2.7"
projects[jquery_update][subdir] = "contrib"

projects[pagerer][version] = "1.1"
projects[pagerer][subdir] = "contrib"

projects[variable][version] = "2.5"
projects[variable][subdir] = "contrib"

projects[better_exposed_filters][version] = "3.6"
projects[better_exposed_filters][subdir] = "contrib"

projects[views][version] = "3.24"
projects[views][subdir] = "contrib"

projects[views_field_view][version] = "1.2"
projects[views_field_view][subdir] = "contrib"

projects[views_fieldsets][version] = "2.3"
projects[views_fieldsets][subdir] = "contrib"

projects[views_slideshow][version] = "3.10"
projects[views_slideshow][subdir] = "contrib"

projects[webform][version] = "4.24"
projects[webform][subdir] = "contrib"

projects[colorbox_node][version] = "3.x-dev"
projects[colorbox_node][subdir] = "contrib"

projects[commerce_cop][version] = "1.0-beta2"
projects[commerce_cop][subdir] = "contrib"

projects[gd_infinite_scroll][version] = "1.7"
projects[gd_infinite_scroll][subdir] = "contrib"

projects[simplemeta][version] = "1.3"
projects[simplemeta][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

; +++++ Themes +++++

; adaptivetheme
projects[adaptivetheme][type] = "theme"
projects[adaptivetheme][version] = "3.4"
projects[adaptivetheme][subdir] = "contrib"

; genesis
projects[genesis][type] = "theme"
projects[genesis][version] = "1.1"
projects[genesis][subdir] = "contrib"

; +++++ Libraries +++++

; ColorBox
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"

; jQuery Colorpicker
libraries[colorpicker][directory_name] = "colorpicker"
libraries[colorpicker][type] = "library"
libraries[colorpicker][destination] = "libraries"
libraries[colorpicker][download][type] = "get"
libraries[colorpicker][download][url] = "http://www.eyecon.ro/colorpicker/colorpicker.zip"
