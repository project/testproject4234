<?php

/**
 * @file
 * Install functions for the profile.
 */

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 */
function food_delivery_profile_install() {
  // Enable some food_delivery_profile blocks.
  $default_theme = 'deliveryfood';
  $admin_theme = 'seven';
  // Disable all themes.
  db_update('system')
    ->fields(array('status' => 0))
    ->condition('type', 'theme')
    ->execute();
  // Enable $default_theme.
  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', $default_theme)
    ->execute();
  // Enable $admin_theme.
  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', $admin_theme)
    ->execute();
  variable_set('theme_default', $default_theme);
  variable_set('admin_theme', $admin_theme);
  // Activate admin theme when editing a node.
  variable_set('node_admin_theme', '1');
  $blocks = array(
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => -10,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
  );
  // Drop system / user blocks to ensure correct building.
  db_delete('block')->condition('module', 'system')->execute();
  db_delete('block')->condition('module', 'user')->execute();
  // Add in our blocks defined above.
  $query = db_insert('block')->fields(array(
    'module',
    'delta',
    'theme',
    'status',
    'weight',
    'region',
    'pages',
    'cache',
  ));
  foreach ($blocks as $block) {
    $query->values($block);
  }
  $query->execute();

  // From shortcut.install, add shortcuts to the default set on install.
  $shortcut_set = shortcut_set_load(SHORTCUT_DEFAULT_SET_NAME);
  $shortcut_set->links = NULL;
  $shortcut_set->links = array(
    array(
      'link_path' => 'admin/appearance/settings',
      'link_title' => st('Theme'),
      'weight' => -17,
    ),
  );
  // Create a default role for site administrators,
  // with all available permissions assigned.
  shortcut_set_save($shortcut_set);
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 10;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();
  // Update the menu router information.
  menu_rebuild();
  // Revert features to ensure they are all installed.
  features_revert_module('commerce_saleprice');

  search_api_index_disable('product_display');

  // Ignore any rebuild messages.
  node_access_needs_rebuild(FALSE);
  // Ignore any other install messages.
  drupal_get_messages();
}

/**
 * Revert search, view, facet and block features.
 */
function food_delivery_profile_update_7100() {
  features_revert(array('search_settings' => array('features', 'strongarm')));
  features_revert(array('views_and_facets' => array('facetapi_defaults', 'features', 'views_default')));
  features_revert(array('blocks_settings' => array('fe_block_settings')));
  drupal_flush_all_caches();
}

/**
 * Revert block feature again.
 */
function food_delivery_profile_update_7101() {
  features_revert(array('blocks_settings' => array('fe_block_settings')));
}
